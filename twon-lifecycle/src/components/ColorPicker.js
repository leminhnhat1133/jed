import { Component } from 'react';
import  './ColorPicker.css';
import classNames from 'classnames';

const RED = 'red';
const GREEN = 'green';
const BLUE = 'blue';
const ORANGE = 'orange';

class ColorPicker extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            currentColor: RED
        }
    }

    setColor = (color) =>{
        this.setState({
            currentColor: color
        });
    }

    render() {
        console.log('Color Picker render...');
        let classColor = this.state.currentColor;
        return (
            <>
                <div>
                    <span 
                        className={classNames('btnRed',{'current': this.state.currentColor === RED })}
                        onClick={ () => this.setColor(RED) }
                    />
                    <span 
                        className={classNames('btnOrange',{'current': this.state.currentColor === ORANGE})}
                        onClick={ () => this.setColor(ORANGE) }
                    />
                    <span 
                        className={classNames('btnGreen',{'current': this.state.currentColor === GREEN})}
                        onClick={ () => this.setColor(GREEN) }
                    />
                    <span
                         className={classNames('btnBlue',{'current': this.state.currentColor === BLUE})}
                         onClick={ () => this.setColor(BLUE) }
                    />
                </div>

                <div className="result">
                    <h1 className={classColor}>TwoN</h1>
                </div>
            </>
        );
    }

    componentDidMount(){
        //goi api
        console.log('Color Picker Did Mount');
    }
    
    componentDidUpdate(){
        console.log('Color Picker Did Update');
    }

    componentWillUnmount(){
        console.log('Color Picker Will Unmount');
    }

    shouldComponentUpdate(nextProps,nextState){
        if(this.state.currentColor === nextState.currentColor)
        {
            return false;
        }
        return true;
    }

}
export default ColorPicker;
