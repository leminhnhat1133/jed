import { Component } from 'react';
import './App.css';
import ColorPicker from './components/ColorPicker';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showColorPicker: true
        }
    }

    handleClick = () => {
        let showColorPicker = this.state.showColorPicker;
        this.setState({
            showColorPicker: !showColorPicker
        });
    }

    render() {
        console.log('App render...');
        let nameButton = this.state.showColorPicker ? 'Đóng Color Picker' : 'Mở Color Picker';
        return (
            <div className="App">
                <button 
                    className="btnShow btn btn-outline-secondary"
                    onClick={() => this.handleClick()}>
                    {nameButton}
                </button>
                {this.state.showColorPicker ? <ColorPicker /> : ''}
            </div>
        );
    }
}
export default App;
