import { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import './App.css'

const CustomLink = ({ title,to , exact }) => {
    return(
        <Route path={to} exact={exact}>
            {({ match })=>{
                var active = match ? 'active' : '';
                return (
                    <li className={active}>
                        <Link to={to}>{title}</Link>
                    </li>
                );
            }}
        </Route>
    );
}

class App extends Component {
    render() {
        return (
           <Router>
                <div className="fuild-container">
                    <nav className="navbar navbar-inverse">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <Link className="navbar-brand" to="/">TwoN Router</Link>
                            </div>
                            <ul className="nav navbar-nav">
                                <CustomLink title='Trang Chủ' to='/' exact={true} />
                                <CustomLink title='Giới Thiệu' to='/about' exact={false} />
                                <CustomLink title='Liên Hệ' to='/contact' exact={false} />
                            </ul>
                        </div>
                    </nav>
                </div>
                <Route path="/" exact component={Home}/>
                <Route path="/about" component={About}/>
                <Route path="/contact" component={Contact}/>
           </Router>
        );
    }
}

export default App;



// Cơ bản
//    <Router>
//         <div className="fuild-container">
//             <nav className="navbar navbar-inverse">
//                 <div className="container-fluid">
//                     <div className="navbar-header">
//                         <Link className="navbar-brand" to="/">TwoN Router</Link>
//                     </div>
//                     <ul className="nav navbar-nav">
//                         <li className='active'>
//                             <Link to='/'>Trang Chủ</Link>
//                         </li>
//                         <li >
//                             <Link to='/about'>Giới Thiệu</Link>
//                         </li>
//                         <li >
//                             <Link to='/contact'>Liên Hệ</Link>
//                         </li>
//                     </ul>
//                 </div>
//             </nav>
//         </div>
//         <Route path="/" exact component={Home}/>
//         <Route path="/about" component={About}/>
//         <Route path="/contact" component={Contact}/>
//    </Router>