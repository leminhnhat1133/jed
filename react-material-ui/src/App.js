import { Grid } from "@material-ui/core";
import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Product from "./components/Product";
import ProductDetail from "./components/Product/detail";

class App extends Component {
    render() {
        return (
            <Router>
                <Grid container direction="column">
                    {/*Header */}
                    <Grid item>
                        <Header/>
                    </Grid>
                    {/*Body */}
                    <Grid item container>
                        <Grid item xs={false} sm={2}></Grid>
                        <Grid item xs={12} sm={8}>
                            <Route path="/" exact component={Product}/>
                            <Route path="/product/:slug" exact component={({match}) =><ProductDetail match={match}/>}/>
                        </Grid>
                        <Grid item xs={false} sm={2}></Grid>
                    </Grid>

                    {/*Footer  */}
                    <Grid item>
                        <Footer/>
                    </Grid>
                </Grid>
            </Router>
            
        );
    }
}

export default App;
