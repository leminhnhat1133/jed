import { AppBar, Toolbar, Typography } from "@material-ui/core";
import React, { Component } from "react";

class Header extends Component {
    render() {
        return (
            <AppBar color="primary" position="static">
                <Toolbar>
                    <Typography>React + Material UI</Typography>
                </Toolbar>
            </AppBar>
        );
    }
}

export default Header;
