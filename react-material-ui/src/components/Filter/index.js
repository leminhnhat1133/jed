import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default function Filter(props) {
    const handle = (event)=>{
        props.receive(event.target.value)
    }
    return (
        <RadioGroup row aria-label="position" name="position" onChange={ (event)=>handle(event)} defaultValue={props.filter} style={{ marginLeft: '10px', display: 'inline-block' }}>
            <b>Lọc: </b>
            <FormControlLabel
                value="all"
                control={<Radio color="primary" />}
                label="Tất Cả"
            />
            <FormControlLabel
                value="1"
                control={<Radio color="primary" />}
                label="Còn Hàng"
            />
            <FormControlLabel
                value="0"
                control={<Radio color="primary" />}
                label="Hết Hàng"
            />
        </RadioGroup>
    );
}