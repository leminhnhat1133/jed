import { Table, TableContainer, TableHead,TableRow,TableCell, Typography, TableBody,Paper } from "@material-ui/core";
import axios from "axios";
import React, {  Component } from "react";
import { Link } from "react-router-dom";
import { orderBy } from 'lodash'
import Sort from "components/Sort";
import Filter from "components/Filter";
import Pagination from "components/Pagination";
class Product extends Component {
    constructor(props) {
        super(props);
        this.state={
            products:[],
            pagination: {
                totalItem: 0,
                activePage: 1,
                itemPerPage: 5,
            },
            sort:{
                title: 'Tên A-Z',
                by:'name',
                value: 'asc'
            },
            filter: 'all'
        }
    }
    componentDidMount(){
        axios.get('http://localhost:3001/products/count').then(res=>{
            this.setState({
                pagination:{
                    ...this.state.pagination,
                    totalItem: res.data.count
                }
            });
        });
        axios.get(`http://localhost:3001/products?page=${this.state.pagination.activePage}&limit=${this.state.pagination.itemPerPage}`).then(res=>{
           this.setState({
               products: res.data
           }) 
        });
    }

    receivePagination = (data)=>{
       this.setState({
            pagination: {
                ...this.state.pagination,
                activePage: data
            }
       })
       axios.get(`http://localhost:3001/products?page=${data}&limit=${this.state.pagination.itemPerPage}`).then(res=>{
            this.setState({
                products: res.data
            }) 
        });
    }

    receiveSort = (data)=>{
        this.setState({
            sort: data
        })
    }

    receiveFilter = (data)=>{
        this.setState({
            filter: data
        })
    }

    render(){
        const listSort=[
            {
                title:'Tên A-Z',
                by:'name',
                value:'asc'
            },
            {
                title:'Tên Z-A',
                by:'name',
                value:'desc'
            },
            {
                title:'ID tăng dần',
                by:'id',
                value:'asc'
            },
            {
                title:'ID giảm dần',
                by:'id',
                value:'desc'
            }
        ];
        const { pagination } = this.state; 
        const totalPage = Math.ceil(pagination.totalItem / pagination.itemPerPage);

        let items=[];

        //Sort
        items= orderBy(this.state.products,[this.state.sort.by],[this.state.sort.value]);

        //Filter
        if(this.state.filter !== 'all')
        {
            items= items.filter(item => item.status === Number(this.state.filter));
        }

        return (
            <div style={{minHeight: '590px'}}>
                <br/>
                <TableContainer style={{minHeight: '500px'}} component={Paper}>
                    <Typography variant="h4" align="center">Danh Sách Sản Phẩm</Typography>
                    <Sort listsort={listSort} sort={this.state.sort} receive={(data)=>this.receiveSort(data)} ></Sort>
                    <Filter filter={this.state.filter} receive={(data)=>this.receiveFilter(data)} ></Filter>
                    <Table style={{minWidth:'650px'}}>
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell>Tên Sản Phẩm</TableCell>
                                <TableCell>Giá</TableCell>
                                <TableCell>Trạng Thái</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {items.map((product,index)=>(
                                <TableRow key={index}>
                                    <TableCell>{product.id}</TableCell>
                                    <TableCell>{product.name}</TableCell>
                                    <TableCell>{product.price} VNĐ</TableCell>
                                    <TableCell>{product.status === 1 ? 'Còn Hàng' :'Hết Hàng'}</TableCell>
                                    <TableCell><Link to={`/product/${product.slug}`}>Xem Chi Tiết</Link></TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    <Pagination page={pagination.activePage} totalPage={totalPage} receive={(data)=>this.receivePagination(data)}/>
                </TableContainer>
                <br/>
            </div>
        );
    }
}

export default Product;
