import { Grid, Paper } from "@material-ui/core";
import axios from "axios";
import React, { Component } from "react";

class ProductDetail extends Component {
    constructor(props) {
        super(props);
        this.state={
            product: {}
        }
    }

    componentDidMount(){
        const { match } = this.props;
        axios.get(`http://localhost:3001/product/${match.params.slug}`).then((res)=>{
            this.setState({
                product : res.data
            });
        })
    }

    render() {
       const product =  this.state.product;
        return (
            <>
                <br/>
                <Paper variant="outlined" style={{minHeight:"549px"}}>
                <center><h3>Chi Tiết Sản Phẩm</h3></center>
                    <Grid container>
                        <Grid item xs={false} sm={2} ></Grid>
                        <Grid item xs={12} sm={8} >
                            <b>Tên:</b><p>{product.name}</p>
                            <b>Giá:</b><p>{product.price} VNĐ</p>
                            <b>Mô Tả:</b><p>{product.desc}</p>
                            <b>Trạng Thái:</b><p>{product.status === 1 ? 'Còn Hàng': 'Hết Hàng'}</p>
                        </Grid>
                        <Grid item xs={false} sm={2} ></Grid>
                    </Grid>
                </Paper>
                <br/>
            </>
        );
    }
}

export default ProductDetail;
