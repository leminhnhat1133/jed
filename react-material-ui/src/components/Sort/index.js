import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

export default function Sort(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  
  const handle = (sort) => {
    setAnchorEl(null);
    props.receive(sort);
  };
  
  return (
    <div style={{display: 'inline-block'}}>
      <Button aria-controls="simple-menu" aria-haspopup="true" variant='outlined' onClick={handleClick}>
        Sắp Xếp: {props.sort.title}
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {props.listsort.map((sort,index)=>{
            return <MenuItem key={index} onClick={()=>handle(sort)}>{sort.title}</MenuItem>
        })}
       
       
      </Menu>
    </div>
  );
}