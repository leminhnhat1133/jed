import React, { Component } from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
class Footer extends Component {
    render() {
        return (
            <AppBar color="primary" position="static">
            <Toolbar>
                <Typography style={{margin: '0 auto'}}>TwoN - Material UI</Typography>
            </Toolbar>
        </AppBar>
        );
    }
}

export default Footer;
