import React, { Component } from 'react';
import { ButtonGroup, Button } from "@material-ui/core";
class Pagination extends Component {

    pre = ()=>{
        const page = this.props.page;
        this.props.receive(page-1);
    }

    next = ()=>{
        const page = this.props.page;
        this.props.receive(page+1);
    }
    render() {
        const {page, totalPage } = this.props;
        return (
            <center>
                <ButtonGroup style={{margin: '20px'}} color="primary" aria-label="outlined primary button group">
                {page === 1 ? 
                <Button disabled={true} onClick={() => this.pre()}> {`<<`} </Button> :
                <Button onClick={() => this.pre()}> {`<<`} </Button>}

                {page === totalPage ?
                <Button disabled={true} onClick={() => this.next()}> {`>>`} </Button> :
                <Button onClick={() => this.next()}> {`>>`} </Button>
                }
                </ButtonGroup>               
            </center>
        );
    }
}

export default Pagination;